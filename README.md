# Arduino HID reverse shell

## How to set it up:
1) Download the files with `git clone https://gitlab.com/WarKitteh/arduino-hid-reverse-shell.git` (or just click the download button on GitLab).
2) Change the IP address (and maybe the port) in `runncat.vbs` on line 3.
3) Upload ncat.exe and runncat.vbs to a file hosting service of your choice. Please note that the file hosting service needs to support direct downloads. If you have your own website/FTP server I would recommend using that (unless you're planning to do shady shit and don't want it to be traced back to you, which I wouldn't recommend to begin with).
4) Change the download links in `arduino-hid-reverse-shell.ino` on line 41. Again, this needs to be a direct download.
5) Uncomment one of the runPayload functions. If you're using a regular Arduino, uncomment line 50. If you're using an Arduino with a button (which I recommend, as this prevents you from running the payload on your own computer), uncomment 55-59. Just solder a button between pin 3 and ground. The payload will only run if you press the button. This way, you can also run the payload again without having to plug the Arduino out and back in (just press the button short).
6) Open `arduino-hid-reverse-shell.ino` in the Arduino IDE and upload the script to your Arduino Pro Micro (or any other Arduino compatible board that supports keyboard emulation).

The victim computer needs to be logged in on an admin account for this to work (as it adds stuff to startup). I've only tested it on Windows 10, but it should at least work on Windows 7 and above.

This will try to connect to your listener every 10 seconds. If you want to change the delay, you can do so on line 4 of `runncat.vbs` (please note that it is in microseconds, so for 1 second, set it to 1000). If you only want it to connect when Windows starts, remove line 1, 4 and 5.

To start a listener, you need to have Ncat installed. On Debian-like systems, just run `apt install nmap` as root. If you're using another Linux distro, MacOS or Windows, just go to [the Nmap download page](https://nmap.org/download.html). If you installed Nmap/Ncat, simply run `ncat -l -p 1234` from the terminal/command prompt. If you're using another port, you obviously need to change that here as well.

You can do this using [NetCat](https://en.wikipedia.org/wiki/Netcat) as well, but the last update for NetCat came out on 2 january 2007. Just saying.